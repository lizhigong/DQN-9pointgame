# DQN-9pointgame

#### 介绍
使用ANN网络实现机器自动玩九宫格游戏，而且越玩越厉害，目前已经很难和AI对弈获胜，要么输要么平。
算法是deepQlearning。

#### 软件架构
棋盘3x3 -1代表空格1代表白棋2代表黑棋 （可以通过设置棋盘大小和获胜连子个数条件改成 五子棋游戏）
神经网络用的ANN网络
算法用的是deenQ-learning 强化学习算法

#### 使用说明
下载后运行AnnNet.py即可

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
